import Fulllayout from '../layouts/fulllayout.jsx';
import Account from '../layouts/account.jsx';

var indexRoutes = [
    { path: '/', name: 'Starter', component: Fulllayout },
	{ path: '/account/', name: 'Starter', component: Account }
];

export default indexRoutes;
